var url = require('url');

function urlParser(reqUrl){
    var id = (url.parse(reqUrl, true).query).id;
    if (id !== undefined) return Promise.resolve(id);
    else return Promise.reject(new Error("Id not defined"));
}

exports.urlParser = urlParser;