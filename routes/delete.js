var express = require('express'),
    router = express.Router(),
    fs = require("fs"),
    path = require("path"),
    fn = require('../lib/numberFiles'),
    up= require('../lib/urlParser');


router.get('/', function (req, res) {
    var ID;
    var filePath = (name) => {
        return path.join(__dirname, `../public/images/${name}.png`)
    };

    var filesRename = (id,number) => {
        if (number !== id) {
            for (var i = id; i <= (number); i++) {
                fs.renameSync(filePath((i * 1) + 1), filePath(i))
            }
        }
    };

    up.urlParser(req.url)
        .then(id => { ID=id;
            return fs.unlinkSync(filePath(id))
        })
        .then(() => {
            return fn.number()
        })
        .then(number => filesRename(ID,number))
        .then(() => res.redirect('/showAll'))
        .catch(error => {
            console.log(error.message);
            res.redirect('/')
    });
});

module.exports = router;