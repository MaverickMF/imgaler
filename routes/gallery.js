var express = require('express');
var router = express.Router();
var fn = require('../lib/numberFiles');
var up= require('../lib/urlParser');

/* GET home page. */
router.get('/', function(req, res) {
    fn.number()
        .then(number => {
        res.cookie('number', `${number}`, {
            expires: new Date(Date.now() + Date.now()),
            httpOnly: false
        });
    })
        .then(()=>{return up.urlParser(req.url)})
        .then(id=>{res.render('gallery', {title: 'Gallery',id:id});
    })
    .catch(()=> {res.render('gallery', {title: 'Gallery',id:0})});
});

module.exports = router;