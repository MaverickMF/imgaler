var express = require('express');
var router = express.Router();
var fn = require('../lib/numberFiles');

/* GET home page. */
router.get('/', function(req, res) {
    fn.number().then(number => {
       res.cookie('number', `${number}`, {
           expires: new Date(Date.now() + Date.now()),
           httpOnly: false
       });
        res.render('index', {title: 'Test'});
    }).catch(error => console.log(error.message));

});

module.exports = router;
