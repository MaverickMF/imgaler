var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
    console.log("Request handler 'start' was called.");
    // language=HTML
    var body = "<html>" +
        '<head>' +
        '<meta http-equiv="Content-Type" ' +
        'content="text/html; charset=UTF-8"/>' +
        '</head>' +
        '<body>' +
        '<h1 style= "text-align: center">Загрузка изображения</h1>' +
        '<form action="/upload" enctype="multipart/form-data" ' +
        'method="post">' +
        '<input type="file" name="upload" multiple  accept="image/png,image/jpeg">' +
        '<input type="submit" value="Upload file"/>' +
        '<br><a href="/">На главную</a><br/>'+
        '</form>' +
        '</body>' +
        '</html>';
    res.send(body);
});

module.exports = router;
