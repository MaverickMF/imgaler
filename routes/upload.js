var express = require('express'),
    router = express.Router(),
    formidable = require("formidable"),
    path = require('path'),
    mv = require('mv'),
    fn = require('../lib/numberFiles')

router.post('/', function (req, res) {
    var form = new formidable.IncomingForm();
    form.multiples = true;
    fn.number().then(fnumber => {
        form.parse(req, function (error, fields, files) {
            if (error) console.log(error.message);
            var arrayFiles = files.upload;
            if (arrayFiles.length === undefined) {
                if (files.upload.name !== "") {
                    moveFile(res, files.upload.path, fnumber);
                    res.redirect("/");
                }
                else res.redirect("/start");
            }
            else {
                if ((arrayFiles.length) > 0) {
                    for (var i = 0; i < arrayFiles.length; i++) {
                        moveFile(res, arrayFiles[i].path, fnumber + i);
                    }
                    res.redirect("/");
                } else res.redirect("/start");
            }
        });

    })
        .catch(error => console.log(error.message));
});

function moveFile(res, fpath, fnumber) {
    mv(fpath, path.join(__dirname, `../public/images/${fnumber + 1}.png`), function (err) {
        if (err) res.send(err);
    });
}

module.exports = router;


